# ---------------------------------------------------------
# Author: Ludovico Frizziero - ludovico.frizziero@gmail.com
# Date: 27/03/2023
# copyright: Ludovico Frizziero
# ---------------------------------------------------------
import gc
from pathlib import Path
from typing import NamedTuple, Optional

import numpy as np
from PIL import Image
from numpy.random import randint
from torch import FloatTensor
from torch.utils.data import Dataset, DataLoader

from utils import Mode, read_data


class Sample(NamedTuple):
    x: np.ndarray
    y: Optional[np.ndarray]


rotations = {
    0: lambda i: i,
    90: lambda i: np.flip(np.transpose(i, axes=[0, 2, 1] if i.ndim == 3 else [1, 0]), axis=1),
    180: lambda i: np.flip(i),
    270: lambda i: np.flip(np.transpose(i, axes=[0, 2, 1] if i.ndim == 3 else [1, 0]), axis=0),
}


class PapirTrain(Dataset):
    __slots__ = 'x', 'y', 'mask', 'height', 'width', 'size', 'len', 'name', 'channels', 'limit'

    def __init__(self, path: Path, size: int = 128, max_samples: int = 1000, channels=None, limit=None, augment=None):
        self.path = path
        self.size = size
        self.len = max_samples
        self.channels = channels
        self.limit = limit
        self.name = str(path.parent.name) + '/' + str(path.name)
        self.x = None
        self.y = None
        self.mask = None
        self.height = -1
        self.width = -1
        self.augment = augment

    def _lazy_load(self):
        data = read_data(self.path, self.channels, self.limit)
        self.x: np.ndarray = data['x']
        self.y: np.ndarray = data['y']
        assert self.y is not None, \
            "This Dataset can only be used for training, hence it is " \
            "expecting valid labels to be supplied alongside the image"
        self.mask: np.ndarray = data['m']

        self.height, self.width = self.x.shape[1:]
        # loose 1 pixel on the right/bottom of the image to be sure not to go out-of-bounds
        # these pixels have little data anyway
        self.height -= self.size // 2 + 1
        self.width -= self.size // 2 + 1

    def __len__(self):
        return self.len

    @staticmethod
    def randomly_rotate(x, y):
        rot = rotations[np.random.randint(0, 4) * 90]
        return rot(x), rot(y)

    def __getitem__(self, idx):
        if self.x is None:
            self._lazy_load()

        half = self.size // 2
        window = None
        while True:
            h = randint(half, self.height)
            w = randint(half, self.width)
            if self.mask[h, w] != 0:
                h -= half
                w -= half
                window = np.s_[..., h:h + self.size, w:w + self.size]
                break

        x = self.x[window].astype(np.float32)
        y = self.y[window]

        if self.augment:
            return self.randomly_rotate(x, y)
        return x, y

    def unload(self):
        self.x = None
        self.y = None
        self.mask = None
        gc.collect()


class PapirInfer(Dataset):
    __slots__ = 'x', 'y', 'mask', 'height', 'width', 'size', 'len', 'name', 'channels', 'limit'

    def __init__(self, path: Path, size: int = 128, channels=None, limit=None):
        self.path = path
        self.size = size
        self.channels = channels
        self.limit = limit
        self.name = str(path.parent.name) + '/' + str(path.name)
        self.x = None
        self.y = None
        self.mask = None
        with Image.open(self.path / 'mask.png') as meta:
            height, width = meta.height, meta.width
        hq, hr = divmod(height, self.size)
        self.height = (hq + int(hr > 0))
        wq, wr = divmod(width, self.size)
        self.width = (wq + int(wr > 0))
        self.len = self.width * self.height

    def _lazy_load(self):
        data = read_data(self.path, self.channels, self.limit)
        self.x: np.ndarray = data['x']
        self.y: np.ndarray = data.get('y')
        self.mask: np.ndarray = data['m']

    def __len__(self):
        return self.len

    def _pad(self, x):
        if len(x.shape) == 2:
            a, b = x.shape
            padding = ((0, self.size - a), (0, self.size - b))
        else:
            _, a, b = x.shape
            padding = ((0, 0), (0, self.size - a), (0, self.size - b))
        if a < self.size or b < self.size:
            x = np.pad(x, padding)
        return x

    def __getitem__(self, i):
        if self.x is None:
            self._lazy_load()
        h, w = np.unravel_index(i, (self.height, self.width))
        _slice = np.s_[..., h * self.size:(h + 1) * self.size, w * self.size:(w + 1) * self.size]
        x = self.x[_slice].astype(float)
        x = self._pad(x)
        if self.y is not None and self.y.size > 0:
            y = self.y[_slice]
            y = self._pad(y)
        else:
            y = None

        return x, y

    def unload(self):
        self.x = None
        self.y = None
        self.mask = None
        gc.collect()


def create_dataloader(
        path: Path,
        size: int = 128,
        mode: Mode = Mode.TRAIN,
        batch_size=32,
        training_samples: int = 1000,
        channels=None,
        limit=None,
):
    if mode is Mode.TRAIN:
        ds = PapirTrain(path, size, training_samples, channels, limit, augment=True)
    else:
        ds = PapirInfer(path, size, channels, limit)
    return DataLoader(
        ds,
        shuffle=mode is Mode.TRAIN,
        batch_size=batch_size,
        drop_last=mode is mode.TRAIN,
        pin_memory=True,
        collate_fn=collate_batch
    )


def collate_batch(samples):
    buffer_x = []
    buffer_y = []
    for x, y in samples:
        buffer_x.append(x)
        if y is not None:
            buffer_y.append(y)

    return FloatTensor(np.stack(buffer_x)), FloatTensor(np.stack(buffer_y)) if buffer_y else None
