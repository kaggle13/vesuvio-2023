# ---------------------------------------------------------
# Author: Ludovico Frizziero - ludovico.frizziero@gmail.com
# Date: 28/03/2023
# copyright: Ludovico Frizziero
# ---------------------------------------------------------
import sys
from pathlib import Path

import torch.cuda

from dataloader import create_dataloader
from loops import LoopConfig, infer
from utils import Mode, load

if __name__ == '__main__':
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    paths = list(str(p) for p in Path(f'artifacts/ResNetGan').glob('*.pt'))
    if len(sys.argv) == 2:
        vers = f"{int(sys.argv[1]):04d}"
        path = next(p for p in paths if vers in p)
    else:
        path = max(paths)
    model, _ = load(path, device)

    paths = [Path('dataset/train/1')] + list(Path('dataset/test').glob('*'))
    infer(
        model,
        dataloaders=[create_dataloader(data, size=32, batch_size=64, mode=Mode.INFER, channels=slice(0, 65, 4))
                     for data in paths],
        cfg=LoopConfig(device=device)
    )
