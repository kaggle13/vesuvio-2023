# ---------------------------------------------------------
# Author: Ludovico Frizziero - ludovico.frizziero@gmail.com
# Date: 28/03/2023
# copyright: Ludovico Frizziero
# ---------------------------------------------------------
import array
from collections import deque
from dataclasses import dataclass
from pathlib import Path
from typing import Union, List

import numpy as np
import torch.utils.data
from PIL import Image
from torch import nn
from torch.optim import Adam
from torch.optim.lr_scheduler import StepLR
from tqdm import tqdm

from utils import save, model_size


@dataclass
class LoopConfig:
    device: str
    accumulation_steps: int = 1
    learning_rate: float = 1
    epochs: int = 1
    checkpoint_path: Union[str, Path] = 'artifacts/ckpt'
    preds_path: Union[str, Path] = 'artifacts/preds'

    def __post_init__(self):
        self.checkpoint_path = Path(self.checkpoint_path)
        self.preds_path = Path(self.preds_path)


def train_one_epoch(model: nn.Module,
                    dataloaders: List[torch.utils.data.DataLoader],
                    optimizer: torch.optim.Optimizer,
                    cfg: LoopConfig,
                    ep: int = None) -> nn.Module:
    tot = sum(len(dl) for dl in dataloaders)
    desc = f'Train "{model.name}" [epoch {{: 2d}} on {{:>10}}]: lr {{:7.6f}}, loss {{:12.6f}}'
    losses = deque(maxlen=100)

    device = cfg.device
    accum = cfg.accumulation_steps
    lr = cfg.learning_rate

    model.train()
    model.to(device)

    optimizer.zero_grad()

    with tqdm(total=tot, desc=desc.format(ep, '<loading>', lr, float('inf'))) as pbar:
        for ck, dl in enumerate(dataloaders, 1):
            stop_idx = len(dl)
            for i, (x, y) in enumerate(dl, 1):
                x = x.to(device, non_blocking=True)
                y = y.to(device, non_blocking=True)

                loss = model.loss(x, y)
                loss /= accum
                losses.append(loss.item())
                loss.backward()

                if i % accum == 0 or i == stop_idx:
                    optimizer.step()
                    optimizer.zero_grad()
                    pbar.update(accum)
                    pbar.set_description(desc.format(ep, dl.dataset.name, lr, np.mean(losses)))

            dl.dataset.unload()
            save(cfg.checkpoint_path, model, vers=ep * 100 + ck, val_loss=float('inf'), verbose=False)

    return model


def train_gan_one_epoch(model: nn.Module,
                        dataloaders: List[torch.utils.data.DataLoader],
                        optimizer_d: torch.optim.Optimizer,
                        optimizer_g: torch.optim.Optimizer,
                        cfg: LoopConfig,
                        ep: int = None) -> nn.Module:
    tot = sum(len(dl) for dl in dataloaders)
    desc = f'Train "{model.name}" [epoch {{: 2d}} on {{:>10}}]: loss D {{:12.6f}}, loss G {{:12.6f}} | D0 before {{:.3f}}, D1 before {{:.3f}}, fake D1 after {{:.3f}}'
    losses_d = deque(maxlen=100)
    losses_g = deque(maxlen=100)

    device = cfg.device
    lr = cfg.learning_rate

    model.train()
    model.to(device)

    with tqdm(total=tot, desc=desc.format(ep, '<loading>', float('inf'), float('inf'), float('inf'), float('inf'),
                                          float('inf'))) as pbar:
        for ck, dl in enumerate(dataloaders, 1):
            for i, (x, y) in enumerate(dl, 1):
                x = x.to(device, non_blocking=True)
                y = y.to(device, non_blocking=True)

                loss_gen = model.loss(x, y)

                optimizer_d.zero_grad()
                loss_d, pred_d1 = next(loss_gen)
                loss_d.backward()
                optimizer_d.step()
                losses_d.append(loss_d.item())

                optimizer_g.zero_grad()
                loss_g, pred_d2 = next(loss_gen)
                loss_g.backward()
                optimizer_g.step()
                losses_g.append(loss_g.item())

                pbar.update(1)
                pred_d1 = torch.sigmoid(pred_d1)
                half = pred_d1.shape[0] // 2
                pred_d1_zero = pred_d1[:half, ...].mean()
                pred_d1_one = pred_d1[half:, ...].mean()
                pbar.set_description(
                    desc.format(ep, dl.dataset.name, np.mean(losses_d), np.mean(losses_g), pred_d1_zero, pred_d1_one,
                                torch.sigmoid(pred_d2).mean()))

            dl.dataset.unload()
            save(cfg.checkpoint_path, model, vers=ep * 100 + ck, val_loss=float('inf'), verbose=False)

    return model


def validate(model, dataloaders: List[torch.utils.data.DataLoader], cfg: LoopConfig) -> float:
    tot = sum(len(dl) for dl in dataloaders)
    desc = f'Eval "{model.name}" [on {{:>10}}]: loss {{:12.6f}}'
    losses = array.array('d')
    device = cfg.device

    model.eval()
    model.to(device)

    with tqdm(total=tot, desc=desc.format('<loading>', float('inf'))) as pbar:
        for dl in dataloaders:
            for x, y in dl:
                x = x.to(device, non_blocking=True)
                y = y.to(device, non_blocking=True)
                loss = model.loss(x, y)
                losses.append(loss.item())
                pbar.update(1)
                pbar.set_description(desc.format(dl.dataset.name, np.mean(losses)))
            dl.dataset.unload()
    return np.mean(losses)


def validate_gan(model, dataloaders: List[torch.utils.data.DataLoader], cfg: LoopConfig) -> float:
    tot = sum(len(dl) for dl in dataloaders)
    desc = f'Eval "{model.name}" [on {{:>10}}]: loss D {{:12.6f}}, loss G {{:12.6f}}'
    losses_d = array.array('d')
    losses_g = array.array('d')
    device = cfg.device

    model.eval()
    model.to(device)

    with tqdm(total=tot, desc=desc.format('<loading>', float('inf'), float('inf'))) as pbar:
        for dl in dataloaders:
            for x, y in dl:
                x = x.to(device, non_blocking=True)
                y = y.to(device, non_blocking=True)
                loss_gen = model.loss(x, y)
                loss_d, pred_d = next(loss_gen)
                loss_g, pred_d = next(loss_gen)
                losses_d.append(loss_d.item())
                losses_g.append(loss_g.item())
                pbar.update(1)
                pbar.set_description(desc.format(dl.dataset.name, np.mean(losses_d), np.mean(losses_g)))
            dl.dataset.unload()
    return np.mean(losses_d) + np.mean(losses_g)


def train(model,
          train_dataloaders: List[torch.utils.data.DataLoader],
          val_dataloaders: List[torch.utils.data.DataLoader],
          cfg: LoopConfig):
    Path(cfg.checkpoint_path).mkdir(parents=True, exist_ok=True)
    msize = model_size(model)

    print(f'Training'.center(100))
    try:
        print(model.info())
    except:
        print(f'''
            Model: {model.name}
            Parameters: trainable {msize[0]:,} | frozen {msize[1] - msize[0]:,} | total {msize[1]:,}
        ''')

    try:
        if hasattr(model, 'gan'):
            optimizer_d = Adam(model.disc.parameters(), lr=cfg.learning_rate * 1e-0)
            optimizer_g = Adam(model.gen.parameters(), lr=cfg.learning_rate)
            scheduler_d = StepLR(optimizer_d, step_size=10, gamma=0.5, verbose=True)
            scheduler_g = StepLR(optimizer_g, step_size=10, gamma=0.5, verbose=True)
            for ep in range(1, cfg.epochs + 1):
                model = train_gan_one_epoch(model, train_dataloaders, optimizer_d, optimizer_g, cfg, ep)
                val_loss = validate_gan(model, val_dataloaders, cfg)
                save(cfg.checkpoint_path, model, vers=ep * 100 + 99, val_loss=val_loss)
                infer(model, val_dataloaders, cfg, ep)
            scheduler_d.step()
            scheduler_g.step()
        else:
            optimizer = Adam(model.parameters(), lr=cfg.learning_rate)
            scheduler = StepLR(optimizer, step_size=10, gamma=0.5, verbose=True)
            for ep in range(1, cfg.epochs + 1):
                model = train_one_epoch(model, train_dataloaders, optimizer, cfg, ep)
                val_loss = validate(model, val_dataloaders, cfg)
                save(cfg.checkpoint_path, model, vers=ep * 100 + 99, val_loss=val_loss)
                infer(model, val_dataloaders, cfg, ep)
            scheduler.step()
    except KeyboardInterrupt:
        pass
    finally:
        save(cfg.checkpoint_path, model, vers=9999, val_loss=float('inf'))


def _infer(model, dataloader: torch.utils.data.DataLoader, cfg: LoopConfig) -> np.ndarray:
    tot = len(dataloader)
    desc = f'Infer "{model.name}" [on {{:>10}}]'

    device = cfg.device

    model.eval()
    model.to(device)

    with tqdm(total=tot, desc=desc.format('<loading>')) as pbar:
        preds = []
        for i, (x, _) in enumerate(dataloader):
            x = x.to(device, non_blocking=True)
            p = model(x)
            p = p.numpy(force=True)
            preds.append(p)
            pbar.update(1)
            pbar.set_description(desc.format(dataloader.dataset.name))
        dataloader.dataset.unload()

    width = dataloader.dataset.width
    height = dataloader.dataset.height
    chunks = []
    for p in preds:
        chunks.extend(np.split(p, p.shape[0], axis=0))  # un-batch predictions

    rows = [np.concatenate(chunks[i:i + width], axis=2) for i in range(0, len(chunks), width)]
    image = np.concatenate(rows, axis=1).squeeze()  # we still have the batch dimension
    print(image.min(), image.max())
    # image[image > 0.5] = 255
    # image[image <= 0.5] = 0
    image *= 255
    return image.astype(np.uint8)


def infer(model, dataloaders: List[torch.utils.data.DataLoader], cfg: LoopConfig, ep: int = None):
    msize = model_size(model)
    print(f'Inference'.center(100))
    if ep is not None:
        try:
            print(model.info())
        except:
            print(f'''
                   Model: {model.name}
                   Parameters: trainable {msize[0]:,} | frozen {msize[1] - msize[0]:,} | total {msize[1]:,}
               ''')

    for dl in dataloaders:
        image = _infer(
            model,
            dataloader=dl,
            cfg=LoopConfig(
                device=cfg.device,
                accumulation_steps=1,
                learning_rate=1e-2,
                epochs=2,
                checkpoint_path=f'artifacts/{model.name}'
            )
        )
        print(image.shape)
        cfg.preds_path.mkdir(parents=True, exist_ok=True)
        suffix = ((f'.ep{ep:03d}' if ep is not None else '') +
                  ('.' + dl.dataset.limit if dl.dataset.limit else '') +
                  '.png')
        img_name = cfg.preds_path / Path(dl.dataset.name).with_suffix(suffix).name
        Image.fromarray(image, mode='L').save(img_name)
        tqdm.write(f"Written {str(img_name)}")
