# ---------------------------------------------------------
# Author: Ludovico Frizziero - ludovico.frizziero@gmail.com
# Date: 28/03/2023
# copyright: Ludovico Frizziero
# ---------------------------------------------------------
from functools import partial

import numpy as np
import torch
from torch import nn
from torch.nn import functional as F

from utils import model_size


class DoubleConv(nn.Module):
    """
    (convolution => [BN] => ReLU) * 2
    adapted from
    https://github.com/milesial/Pytorch-UNet/blob/67bf11b4db4c5f2891bd7e8e7f58bcde8ee2d2db/unet/unet_parts.py
    """

    def __init__(self, in_channels, out_channels, kernel_size=None, mid_channels=None):
        super().__init__()
        mid_channels = mid_channels or out_channels
        kernel_size = kernel_size or 3
        self.double_conv = nn.Sequential(
            nn.Conv2d(in_channels, mid_channels, kernel_size=kernel_size, padding=1),
            # nn.BatchNorm2d(mid_channels),
            nn.ReLU(inplace=True),
            nn.Conv2d(mid_channels, out_channels, kernel_size=kernel_size, padding=1),
            # nn.BatchNorm2d(out_channels),
            nn.ReLU(inplace=True)
        )

    def forward(self, x):
        return self.double_conv(x)


class Down(nn.Module):
    """
    Downscaling with maxpool then double conv
    adapted from
    https://github.com/milesial/Pytorch-UNet/blob/67bf11b4db4c5f2891bd7e8e7f58bcde8ee2d2db/unet/unet_parts.py
    """

    def __init__(self, in_channels, out_channels, kernel_size=None):
        super().__init__()
        self.maxpool_conv = nn.Sequential(
            nn.MaxPool2d(2),
            DoubleConv(in_channels, out_channels, kernel_size)
        )

    def forward(self, x):
        return self.maxpool_conv(x)


class Up(nn.Module):
    """
    Upscaling then double conv
    adapted from
    https://github.com/milesial/Pytorch-UNet/blob/67bf11b4db4c5f2891bd7e8e7f58bcde8ee2d2db/unet/unet_parts.py
    """

    def __init__(self, in_channels, out_channels, kernel_size=None, bilinear=True):
        super().__init__()
        # if bilinear, use the normal convolutions to reduce the number of channels
        if bilinear:
            self.up = nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True)
            self.conv = DoubleConv(in_channels, out_channels, kernel_size, in_channels // 2)
        else:
            self.up = nn.ConvTranspose2d(in_channels, in_channels // 2, kernel_size=2, stride=2)
            self.conv = DoubleConv(in_channels, out_channels, kernel_size)

    def forward(self, x1, x2):
        x1 = self.up(x1)
        # input is C x H x W
        diffY = x2.size()[2] - x1.size()[2]
        diffX = x2.size()[3] - x1.size()[3]

        x1 = F.pad(x1, [diffX // 2, diffX - diffX // 2,
                        diffY // 2, diffY - diffY // 2])
        # if you have padding issues, see
        # https://github.com/HaiyongJiang/U-Net-Pytorch-Unstructured-Buggy/commit/0e854509c2cea854e247a9c615f175f76fbb2e3a
        # https://github.com/xiaopeng-liao/Pytorch-UNet/commit/8ebac70e633bac59fc22bb5195e513d5832fb3bd
        x = torch.cat([x2, x1], dim=1)
        return self.conv(x)


class OutConv(nn.Module):
    """
    Final convolution, to create the output
    adapted from
    https://github.com/milesial/Pytorch-UNet/blob/67bf11b4db4c5f2891bd7e8e7f58bcde8ee2d2db/unet/unet_parts.py
    """

    def __init__(self, in_channels, out_channels):
        super(OutConv, self).__init__()
        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size=1)

    def forward(self, x):
        return self.conv(x)


class Unet(nn.Module):
    __slots__ = (
        'down1', 'down2', 'down3', 'down4',
        'up1', 'up2', 'up3', 'up4',
        'inc', 'outc', 'crit', 'sigmoid'
    )

    name = 'Unet'

    def __reduce__(self):
        return self.__class__, (self.bilinear, self.kernel_size, self.channels)

    def __init__(self, bilinear=True, kernel_size=3, channels=17):
        super(Unet, self).__init__()
        self.bilinear = bilinear
        self.kernel_size = kernel_size
        self.channels = channels
        c = 65

        ks = kernel_size
        bil = bilinear

        self.inc = DoubleConv(channels, c, kernel_size=ks)
        self.down1 = Down(c, c * 2, ks)
        self.down2 = Down(c * 2, c * 4, ks)
        self.down3 = Down(c * 4, c * 8, ks)
        factor = 2 if bil else 1
        self.down4 = Down(c * 8, (c * 16) // factor, ks)
        self.up1 = Up(c * 16, (c * 8) // factor, ks, bil)
        self.up2 = Up(c * 8, (c * 4) // factor, ks, bil)
        self.up3 = Up(c * 4, (c * 2) // factor, ks, bil)
        self.up4 = Up(c * 2, c, ks,
                      bil)  # Up actually concats prev tensor which has 64 channels with lvl1 downscaling tensor, which has 65 channels, hence total of 129
        self.outc = OutConv(c, 1)
        self.crit = nn.BCEWithLogitsLoss()
        self.sigmoid = nn.Sigmoid()

    def logits(self, x):
        x = x / 65536.0

        x1 = self.inc(x)
        x2 = self.down1(x1)
        x3 = self.down2(x2)
        x4 = self.down3(x3)
        x5 = self.down4(x4)
        x = self.up1(x5, x4)
        x = self.up2(x, x3)
        x = self.up3(x, x2)
        x = self.up4(x, x1)
        logits = self.outc(x)
        return logits.squeeze()

    def forward(self, x):
        x = self.logits(x)
        return self.sigmoid(x)

    def loss(self, x, y):
        return self.crit(self.logits(x), y)


class ResNetLayer(nn.Module):
    __slots__ = 'residual', 'conv', 'relu'

    def __init__(self, in_channels, out_channels, kernel_size):
        super().__init__()
        mid_channels = max(1, out_channels // 2)
        self.residual = in_channels == out_channels
        self.conv = nn.Sequential(
            nn.Conv2d(in_channels, mid_channels, kernel_size=1),
            nn.BatchNorm2d(mid_channels),
            nn.ReLU(inplace=True),
            nn.Conv2d(mid_channels, mid_channels, kernel_size=kernel_size, padding='same', padding_mode='reflect'),
            nn.BatchNorm2d(mid_channels),
            nn.ReLU(inplace=True),
            nn.Conv2d(mid_channels, out_channels, kernel_size=1),
            nn.BatchNorm2d(out_channels),
        )
        self.relu = nn.ReLU(inplace=True)

    def forward(self, x):
        t = self.conv(x)
        if self.residual:
            t += x
        return self.relu(t)


class DownRecasting(nn.Module):
    __slots__ = 'recast',

    def __reduce__(self):
        return self.__class__, (self.inc, self.outc, self.act)

    def __init__(self, in_channels, out_channels, activation=True):
        super().__init__()
        self.inc = in_channels
        self.outc = out_channels
        self.act = activation
        self.recast = nn.Sequential(
            nn.MaxPool2d(2),
            nn.Conv2d(in_channels, out_channels, kernel_size=1),
            nn.BatchNorm2d(out_channels),
        )
        if activation:
            self.recast.append(nn.ReLU(inplace=True))

    def forward(self, x):
        return self.recast(x)


class ResNet(nn.Module):
    __slots__ = 'in_size', 'net', 'sigmoid', 'crit',
    name = 'ResNet'

    def __reduce__(self):
        return self.__class__, (self.channels, self.in_size, self.kernel_size, self.replication)

    def __init__(self, starting_channels=65, in_size=32, kernel_size=3, replication=0):
        super().__init__()
        c = starting_channels if starting_channels > 64 else 64
        n_max_pool = 4
        assert in_size >= 32 and int(np.log2(in_size)) == np.log2(in_size), \
            "in_size must be a power of 2, and at least 32"
        self.in_size = in_size
        self.channels = starting_channels
        self.kernel_size = kernel_size
        self.replication = replication
        self.net = nn.Sequential(
            ResNetLayer(starting_channels, c, kernel_size),
            *[ResNetLayer(c, c, kernel_size) for _ in range(replication)],
            DownRecasting(c, c * 2),  # >16

            ResNetLayer(c * 2, c * 2, kernel_size),
            *[ResNetLayer(c * 2, c * 2, kernel_size) for _ in range(replication)],
            DownRecasting(c * 2, c * 4),  # >8

            ResNetLayer(c * 4, c * 4, kernel_size),
            *[ResNetLayer(c * 4, c * 4, kernel_size) for _ in range(replication)],
            DownRecasting(c * 4, c * 8),  # >4

            ResNetLayer(c * 8, c * 8, 3),
            *[ResNetLayer(c * 8, c * 8, 3) for _ in range(replication)],
            DownRecasting(c * 8, c * 16),  # >2

            ResNetLayer(c * 16, c * 16, 3),
            *[ResNetLayer(c * 16, c * 16, 3) for _ in range(replication)],
            DownRecasting(c * 16, c * 16),

            nn.Flatten(start_dim=1),
            nn.Linear(c * 16, self.in_size ** 2),
        )
        self.sigmoid = nn.Sigmoid()
        self.crit = nn.BCEWithLogitsLoss()

    def logits(self, x):
        x = x / 65536.0
        x = self.net(x)
        return x.view(-1, self.in_size, self.in_size)

    def forward(self, x):
        x = self.logits(x)
        return self.sigmoid(x)

    def loss(self, x, y):
        return self.crit(self.logits(x), y)


class ResNetGan(nn.Module):
    __slots__ = 'in_size', 'gen', 'disc', 'sigmoid', 'crit',
    name = 'ResNetGan'
    gan = True

    def __reduce__(self):
        return self.__class__, (self.channels, self.in_size, self.kernel_size, self.replication)

    def __init__(self, starting_channels=65, in_size=32, kernel_size=3, replication=0):
        super().__init__()
        c = starting_channels if starting_channels > 75 else 75
        n_max_pool = 4
        assert in_size >= 32 and int(np.log2(in_size)) == np.log2(in_size), \
            "in_size must be a power of 2, and at least 32"
        self.in_size = in_size
        self.channels = starting_channels
        self.kernel_size = kernel_size
        self.replication = replication

        self.gen = nn.Sequential(
            ResNetLayer(starting_channels, c, kernel_size),
            *[ResNetLayer(c, c, kernel_size) for _ in range(replication)],
            DownRecasting(c, c * 2),  # >16

            ResNetLayer(c * 2, c * 2, kernel_size),
            *[ResNetLayer(c * 2, c * 2, kernel_size) for _ in range(replication)],
            DownRecasting(c * 2, c * 4),  # >8

            ResNetLayer(c * 4, c * 4, kernel_size),
            *[ResNetLayer(c * 4, c * 4, kernel_size) for _ in range(replication)],
            DownRecasting(c * 4, c * 8),  # >4

            ResNetLayer(c * 8, c * 8, kernel_size),
            *[ResNetLayer(c * 8, c * 8, kernel_size) for _ in range(replication)],
            DownRecasting(c * 8, c * 16),  # >2

            ResNetLayer(c * 16, c * 16, kernel_size),
            *[ResNetLayer(c * 16, c * 16, kernel_size) for _ in range(replication)],
            DownRecasting(c * 16, c * 16),

            nn.Flatten(start_dim=1),
            nn.Linear(c * 16, self.in_size ** 2),
        )

        self.disc = nn.Sequential(
            ResNetLayer(1, 8, kernel_size=3),
            DownRecasting(8, 16),
            ResNetLayer(16, 32, kernel_size=3),
            DownRecasting(32, 64),
            nn.Flatten(start_dim=1),
            nn.Linear(((self.in_size // 4) ** 2) * 64, 512),
            nn.ReLU(inplace=True),
            nn.Linear(512, 32),
            nn.ReLU(inplace=True),
            nn.Linear(32, 1),
        )

        self.sigmoid = nn.Sigmoid()
        self.crit = nn.BCEWithLogitsLoss()

    def info(self):
        _, totd = model_size(self.disc)
        _, totg = model_size(self.gen)
        return f'''
            Model: {self.name}
            Parameters: discriminator {totd:,} | generator {totg:,} | total {totg + totd:,}
        '''

    def logits(self, x):
        x = x / 65536.0
        x = self.gen(x)
        return x.view(-1, self.in_size, self.in_size)

    def forward(self, x):
        x = self.logits(x)
        return self.sigmoid(x)

    def loss(self, x, y):
        one = torch.ones(y.shape[0], 1, device='cuda' if torch.cuda.is_available() else 'cpu')
        zero = torch.zeros(y.shape[0], 1, device='cuda' if torch.cuda.is_available() else 'cpu')

        logits = self.logits(x)
        fake = self.sigmoid(logits).unsqueeze(1)

        x_disc = torch.cat((fake.detach(), y.unsqueeze(1)))
        y_disc = torch.cat((zero, one))
        pred_d = self.disc(x_disc)
        loss_d = self.crit(pred_d, y_disc)
        yield loss_d, pred_d

        # gen's target is to fool the disc --> use one as labels
        pred_d = self.disc(fake)
        loss_g = 1e-3 * self.crit(pred_d, one) + self.crit(logits, y)
        yield loss_g, pred_d
