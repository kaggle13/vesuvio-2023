# ---------------------------------------------------------
# Author: Ludovico Frizziero - ludovico.frizziero@gmail.com
# Date: 27/03/2023
# copyright: Ludovico Frizziero
# ---------------------------------------------------------
from pathlib import Path
from typing import Union

import numpy as np
import yaml
from PIL import Image
from tqdm import tqdm


def crop(root: Path, dest: Path, compressed=False, metadata=True):
    count = 1
    for d in root.iterdir():
        if not d.is_dir():
            continue

        try:
            with Image.open(d / 'inklabels.png') as labels:
                labels = labels.convert('L')
            labels = np.asarray(labels).copy()
            labels[labels > 0] = 1
        except FileNotFoundError:
            labels = None

        with Image.open(d / 'mask.png') as mask:
            mask = np.asarray(mask.convert('L')).copy()
            mask[mask > 0] = 1

        dest.mkdir(parents=True, exist_ok=True)

        papir = d / 'surface_volume'

        buffer = []
        for surface in tqdm(papir.iterdir(), total=65, desc=str(d.resolve())):
            with Image.open(surface) as s:
                buffer.append(np.asarray(s))

        volume = np.stack(buffer)
        w = volume.shape[-1] // 2
        for k in range(2):
            _slice = np.s_[..., k * w:(k + 1) * w]
            kw = dict(x=volume[_slice], y=labels[_slice] if labels is not None else np.empty(0), m=mask[_slice])
            if compressed:
                print(f'starting compression of part {k}')
                np.savez_compressed(dest / f'data{count:02d}.npz', **kw)
            else:
                np.savez(dest / f'data{count:02d}.unc.npz', **kw)
            if metadata:
                channels, height, width = volume[_slice].shape
                with open(dest / f'data{count:02d}.yml', 'w') as f:
                    yaml.dump({'height': height, 'width': width, 'channels': channels}, f)
            count += 1


if __name__ == '__main__':
    crop(Path('dataset/train'), Path('dataset/tiles/train'), True, metadata=True)
    crop(Path('dataset/test'), Path('dataset/tiles/test'), True, metadata=True)
