# ---------------------------------------------------------
# Author: Ludovico Frizziero - ludovico.frizziero@gmail.com
# Date: 28/03/2023
# copyright: Ludovico Frizziero
# ---------------------------------------------------------
from pathlib import Path

import torch.cuda

from dataloader import create_dataloader
from loops import train, LoopConfig
from nn import ResNet, ResNetGan, Unet
from utils import Mode

if __name__ == '__main__':
    ks = 3
    bs = 64
    ch = slice(0, 65, 4)
    nch = len(list(range(65))[ch])
    size = 32
    epochs = 30

    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    # model = Unet(bilinear=True, kernel_size=ks, channels=nch)
    # model = ResNet(starting_channels=nch, in_size=size, kernel_size=ks, replication=0)
    model = ResNetGan(starting_channels=nch, in_size=size, kernel_size=ks, replication=0)

    paths = sorted(Path('dataset/train').glob('*'))
    print('Working on\n   ' + '\n   '.join(str(p) for p in paths))
    train(
        model,
        train_dataloaders=[
            create_dataloader(p, size=size, batch_size=bs, mode=Mode.TRAIN, training_samples=bs * 1000,
                              channels=ch, limit=l)
            for p, l in zip(paths, [None, None, 'upper_half'])],
        val_dataloaders=[
            create_dataloader(paths[-1], size=size, batch_size=bs, mode=Mode.TEST, channels=ch, limit='lower_half'),
        ],
        cfg=LoopConfig(
            device=device,
            accumulation_steps=1,
            learning_rate=1e-2,
            epochs=epochs,
            checkpoint_path=f'artifacts/{model.name}'
        )
    )
