# ---------------------------------------------------------
# Author: Ludovico Frizziero - ludovico.frizziero@gmail.com
# Date: 27/03/2023
# copyright: Ludovico Frizziero
# ---------------------------------------------------------
import warnings
from enum import Enum
from pathlib import Path
from typing import Union, Any, Optional

import numpy as np
import torch
from PIL import Image
from tqdm import tqdm


class Mode(Enum):
    TRAIN = 'TRAIN'
    TEST = 'TEST'
    INFER = 'INFER'


def model_size(model) -> (int, int):
    counts = [(p.numel(), p.requires_grad) for p in model.parameters()]
    return sum(x for x, grad in counts if grad), sum(x for x, _ in counts)


def load(path: Union[str, Path], device) -> (Any, torch.nn.Module, float):
    x = torch.load(path, map_location=device)
    model: torch.nn.Module = x['model']
    model.load_state_dict(x['model_state_dict'])
    print(f'[load] Loaded model {path}')
    return model, x['val_loss']


def save(directory: Union[str, Path], model: torch.nn.Module, vers: int = 1, val_loss: float = float('inf'),
         verbose=True):
    directory = Path(directory)
    directory.mkdir(exist_ok=True)
    fname = directory / f'{model.name}.{vers:04d}.pt'
    torch.save(
        {
            'model_state_dict': model.state_dict(),
            'model': model,
            'val_loss': val_loss
        },
        fname
    )
    if verbose:
        tqdm.write(f'Saved to {fname}')


def _open_image(p: Path, mode=None) -> np.ndarray:
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        with Image.open(p) as i:
            if mode:
                i = i.convert(mode)
            return np.asarray(i)


def read_data(root: Path, channels: Union[int, slice] = slice(0, 65, 2), limit: Optional[str] = None) -> dict:
    mask = _open_image(root / 'mask.png', mode='L')
    mask = np.asarray(mask)

    h, w = mask.shape
    if limit == 'upper_half':
        _slice = np.s_[:h // 2, :]
    elif limit == 'lower_half':
        _slice = np.s_[h // 2:, :]
    else:
        _slice = np.s_[...]

    mask = mask[_slice].copy()
    mask[mask > 0] = 1

    try:
        labels = _open_image(root / 'inklabels.png', mode='L')
        labels = np.asarray(labels)[_slice].copy()
        labels[labels > 0] = 1
    except FileNotFoundError:
        labels = None

    if isinstance(channels, int):
        channels = slice(0, channels)
    channels = list(range(65))[channels]

    paths = [root / 'surface_volume' / f'{c:02d}.tif' for c in channels]
    images = []
    for p in paths:
        images.append(_open_image(p)[_slice])

    return {'x': np.stack(images), 'y': labels, 'm': mask}
